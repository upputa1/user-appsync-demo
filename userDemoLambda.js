const AWS = require('aws-sdk');

console.log('Loading function');

const tableName = 'user'

const db = new AWS.DynamoDB({});
const dynamodb = new AWS.DynamoDB.DocumentClient({
    service: db,
    region: "us-east-1"
});

exports.handler = async (event, context) => {

    console.log(`## event: ${JSON.stringify(event)}`);

    switch (event.info.fieldName) {
        case 'createUser':
            const params = {
                TableName: "user",
                Item: event.arguments,
                ReturnValues: "ALL_OLD"
            };

            const result = await dynamodb.put(params).promise();
            console.log(result)
            return event.arguments;
            break;

        // code
        case 'listUsers':
            const queryParams = {
                TableName: "user"
            };
            const queryResults = await dynamodb.scan(queryParams).promise()
            console.log(JSON.stringify(queryResults))
            return queryResults.Items;

        default:
            return "Nothing is set";
    }
};
