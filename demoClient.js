// import { ApolloClient, InMemoryCache } from '@apollo/client';

// import { gql } from '@apollo/client';
require('node-fetch');
require('isomorphic-fetch');
const gql = require('graphql-tag');
const AWSAppSyncClient = require('aws-appsync').default;
const AUTH_TYPE = require('aws-appsync/lib/link/auth-link').AUTH_TYPE;

// If you want to use AWS...
const AWS = require('aws-sdk');

require('es6-promise').polyfill();


const query = gql(`
query list {  
  listUsers {   
    id,
    name
  }
}
`);

const client = new AWSAppSyncClient({
    url: "https://566vhsi5ibcfreipbgsze6xgn4.appsync-api.us-east-1.amazonaws.com/graphql",
    region: "us-east-1",
    auth: {
        type: AUTH_TYPE.API_KEY,
        apiKey: "da2-zwywpfvmevaapojopjue6vfri4",
    },

    disableOffline: true      //Uncomment for AWS Lambda
});

client.hydrated().then(function (client) {
    // Now run a query
    client.query({ query: query})
        .then(function log(data) {
            console.log('results of query: ', JSON.stringify(data));
        })
        .catch(console.error);
});
